var assign = require('object-assign');
var galv = require('galvatron');
var gat = require('gulp-auto-task');
var gulp = require('gulp');
var gulpWebserver = require('gulp-webserver');

var opts = gat.opts();
var taskFlatapp = gat.load('flatapp');

module.exports = gulp.series(
    taskFlatapp,
    function flatappWatch (done) {
        gulp.watch([
            'src/**',
            'tests/flatapp/src/**',
            'tests/test-pages/**'
        ], taskFlatapp).on('change', galv.cache.expire);
        done();
    },
    function flatappServer () {
        opts = assign({
            host: '0.0.0.0',
            port: 7000
        }, opts);
        return gulp.src('.tmp/flatapp/target/static')
            .pipe(gulpWebserver({
                host: opts.host,
                livereload: false,
                open: 'pages',
                port: opts.port
            }));
    }
);
