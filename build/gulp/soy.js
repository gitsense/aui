'use strict';

var glob = require('glob');
var opts = require('gulp-auto-task').opts();
var path = require('path');
var sh = require('shelljs');
var soyCli = require('atlassian-soy-cli');
var soyVersion = require(path.join('@atlassian', 'soy-template-plugin-js', 'package.json')).version;
var wrap = require('../lib/wrap-file');

// Fix so that soy code exports properly to the window when wrapped in an iife.
var wrapAui = wrap('(function(){\naui = window.aui = window.aui || {}\n', '\n}());');
var wrapAtl = wrap('(function(){\n', '\n}());');
var wrapGoog = wrap('(function(){\ngoog = window.goog = window.goog || {};\n', '\nwindow.soy = soy;\nwindow.soydata = soydata;\n}());');

module.exports = function (done) {
    sh.rm('-rf', '.tmp/compiled-soy');

    var cli = soyCli({
        version: soyVersion,
        basedir: path.join(opts.root, 'src/soy'),
        outdir: '.tmp/compiled-soy',
        extraArgs: {
            i18n: path.join(opts.root, 'src/i18n/aui.properties')
        }
    });

    cli.compile('**.soy').then(function() {
        glob('.tmp/compiled-soy/*.js', function (error, files) {
            if (error) {
                throw error;
            }
            files.forEach(function(file) {
                wrapAui(file);
            });

            wrapAtl(require.resolve('@atlassian/soy-template-plugin-js/src/js/atlassian-deps.js'), '.tmp/compiled-soy/atlassian-deps.js');
            wrapGoog(require.resolve('@atlassian/soy-template-plugin-js/src/js/soyutils.js'), '.tmp/compiled-soy/soyutils.js');

            done();
        });
    }, done);
};
