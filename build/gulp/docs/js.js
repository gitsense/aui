'use strict';

var galv = require('galvatron');
var gulp = require('gulp');
var gulpBabel = require('gulp-babel');
var gulpConcat = require('gulp-concat');
var gulpDebug = require('gulp-debug');
var gulpIf = require('gulp-if');
var gulpUglify = require('gulp-uglify');
var isEs6 = require('../../lib/is-es6');
var lazyPipe = require('lazypipe');
var libTraceMap = require('../../lib/trace-map');
var libVersionReplace = require('../../lib/version-replace');
var opts = require('gulp-auto-task').opts();

module.exports = function docsJs () {
    var shouldMinify = !opts['no-minify'];

    var babelify = lazyPipe()
        .pipe(gulpDebug, {title: 'babel'})
        .pipe(galv.cache, 'babel', gulpBabel());

    return galv.trace('docs/src/scripts/index.js', {
        map: libTraceMap(opts.root)
    }).createStream()
        .pipe(libVersionReplace())
        .pipe(gulpIf(isEs6, babelify()))
        .pipe(galv.cache('globalize', galv.globalize()))
        .pipe(gulpConcat('index.js'))
        .pipe(gulpIf(shouldMinify, gulpUglify()))
        .pipe(gulp.dest('.tmp/docs/dist/scripts'));
};
