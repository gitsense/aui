'use strict';

var glob = require('glob');
var path = require('path');

var buildPath = 'build/gulp';
var log = console.log.bind(console);

module.exports = function (done) {
    log('Available Tasks');
    log('---------------');
    log();
    log('To see specific information about a task call the task with `--help`.');

    glob.sync(path.join(buildPath, '{*,**/*}.js')).forEach(function (file) {
        var name = file.replace(buildPath + '/', '').replace('.js', '');
        log('  ' + name);
    });

    done();
};
