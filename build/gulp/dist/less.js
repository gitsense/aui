var galv = require('galvatron');
var gat = require('gulp-auto-task');
var gulp = require('gulp');
var gulpDebug = require('gulp-debug');
var gulpIf = require('gulp-if');
var gulpRename = require('gulp-rename');
var gulpLess = require('gulp-less');
var minifyCss = require('../../lib/minify-css');
var rootPaths = require('../../lib/root-paths');

var opts = gat.opts();

module.exports = gulp.parallel(
    function fonts () {
        return gulp.src(rootPaths('src/less/fonts/**'))
            .pipe(gulpDebug({title: 'fonts'}))
            .pipe(gulp.dest('dist/aui/css/fonts'));
    },
    function less () {
        var shouldMinify = !opts['no-minify'];
        return gulp.src('src/less/batch/*.less')
            .pipe(gulpDebug({title: 'less'}))
            .pipe(galv.cache('less', gulpLess({paths: ['src/less/batch']})))
            .pipe(gulp.dest('dist/aui/css'))
            .pipe(gulp.dest('.tmp/dist/aui/css'))
            .pipe(gulpIf(shouldMinify, minifyCss()))
            .pipe(gulpIf(shouldMinify, gulpRename({suffix: '.min'})))
            .pipe(gulpIf(shouldMinify, gulp.dest('dist/aui/css')))
            .pipe(gulpIf(shouldMinify, gulp.dest('.tmp/dist/aui/css')));
    },
    function mainImages () {
        return gulp.src(rootPaths('src/less/images/**/*'))
            .pipe(gulpDebug({title: 'images'}))
            // We have to pipe to both css/images and images because paths are
            // referenced differently in the refapp and in our css.
            .pipe(gulp.dest('dist/aui/css/images'))
            .pipe(gulp.dest('dist/aui/css'))
            .pipe(gulp.dest('.tmp/dist/aui/css/images'))
            .pipe(gulp.dest('.tmp/dist/aui/css'));
    },
    function select2Images () {
        var gifPaths = rootPaths('src/css-vendor/jquery/plugins/*.gif');
        var pngPaths = rootPaths('src/css-vendor/jquery/plugins/*.png');
        return gulp.src(gifPaths.concat(pngPaths))
            .pipe(gulpDebug({title: 'select2'}))
            .pipe(gulp.dest('dist/aui/css'))
            .pipe(gulp.dest('.tmp/dist/aui/css'));
    }
);
